let playerPosition = {linha: 0, coluna: 0};

const map = [
  "WWWWWWWWWWWWWWWWWWWWW",
  "W   W     W     W W W",
  "W W W WWW WWWWW W W W",
  "W W W   W     W W   W",
  "W WWWWWWW W WWW W W W",
  "W         W     W W W",
  "W WWW WWWWW WWWWW W W",
  "W W   W   W W     W W",
  "W WWWWW W W W WWW W F",
  "S     W W W W W W WWW",
  "WWWWW W W W W W W W W",
  "W     W W W   W W W W",
  "W WWWWWWW WWWWW W W W",
  "W       W       W   W",
  "WWWWWWWWWWWWWWWWWWWWW",
];


function sketchLab(){

  let labArea = document.getElementById("labArea");
  labArea.textContent = "";

  for (let i = 0; i < map.length; i++){

    let labLine = document.createElement("div");
    labLine.id = `line${i}`;
    labLine.style = 'display: flex'; 
    labArea.appendChild(labLine); 

    for (let k = 0; k < map[i].length; k++){

      if (map[i][k] === 'W'){
          let line = document.getElementById(`line${i}`);
          let wallBlock = document.createElement("div");
          wallBlock.classList.add('wallBlocks');
          line.appendChild(wallBlock); 
      } else if (map[i][k] === ' '){
          let line = document.getElementById(`line${i}`);
          let pathBlock = document.createElement("div");
          pathBlock.classList.add('pathBlocks');
          line.appendChild(pathBlock);
      } else if (map[i][k] === 'S'){
          let line = document.getElementById(`line${i}`);
          let pathBlock = document.createElement("div");
          pathBlock.id = 'player';
          line.appendChild(pathBlock);

          playerPosition['linha'] = i;
          playerPosition['coluna'] = k;  
      }
    }
  }

}
sketchLab();

document.addEventListener("keydown", (event) => {
  let wallAlert = document.getElementById('wallAlert');
  wallAlert.textContent = '';

  if (event.key === 'ArrowRight'){

    if (map[playerPosition['linha']][playerPosition['coluna'] + 1] === "W" || map[playerPosition['linha']][playerPosition['coluna'] + 1] !== " " && map[playerPosition['linha']][playerPosition['coluna'] + 1] !== "F"){
      wallAlert.textContent = 'Ops... isto é uma parede!';
      return;
    }

    if (map[playerPosition['linha']][playerPosition['coluna'] + 1] === "F"){
      let modal = document.createElement('div');
      modal.id = 'modal';

      let title = document.createElement('h2');
      title.textContent = 'Parabéns, você saiu do labirinto!';
      modal.appendChild(title);

      let btReset = document.createElement('button');
      btReset.textContent = 'Jogar Novamente';
      modal.appendChild(btReset);

      document.body.appendChild(modal); 

      btReset.addEventListener('click', reload);
    }

    let newStr0 = '';
    newStr0 = map[playerPosition['linha']].replace('S', ' ');   
    map.splice([playerPosition['linha']], 1, newStr0); 
    playerPosition['coluna'] += 1; 

    let strArr = newStr0.split("");
    strArr.splice(playerPosition['coluna'], 1, 'S');
    let newStr1 = strArr.join("");
    map.splice(playerPosition['linha'], 1, newStr1); 

    sketchLab();
  } 

  if (event.key === 'ArrowLeft'){

    if (map[playerPosition['linha']][playerPosition['coluna'] - 1] === "W" || map[playerPosition['linha']][playerPosition['coluna'] - 1] !== " "){
      wallAlert.textContent = 'Ops... isto é uma parede!';
      return;
    }

    let newStr0 = '';
    newStr0 = map[playerPosition['linha']].replace('S', ' ');   
    map.splice([playerPosition['linha']], 1, newStr0); 
    playerPosition['coluna'] -= 1; 

    let strArr = newStr0.split("");
    strArr.splice(playerPosition['coluna'], 1, 'S');
    let newStr1 = strArr.join("");
    map.splice(playerPosition['linha'], 1, newStr1); 

    sketchLab();
  }

  if (event.key === 'ArrowUp'){

    if (map[playerPosition['linha'] - 1][playerPosition['coluna']] === "W"){
      wallAlert.textContent = 'Ops... isto é uma parede!';
      return;
    }

    let newStr0 = '';
    newStr0 = map[playerPosition['linha']].replace('S', ' ');   
    map.splice(playerPosition['linha'], 1, newStr0); 
    playerPosition['linha'] -= 1; 

    let newStr1 = map[playerPosition['linha']];
    let strArr = newStr1.split("");
    strArr.splice(playerPosition['coluna'], 1, 'S');
    let newStr2 = strArr.join("");
    map.splice(playerPosition['linha'], 1, newStr2); 

    sketchLab();
  }

  if (event.key === 'ArrowDown'){

    if (map[playerPosition['linha'] + 1][playerPosition['coluna']] === "W"){
      wallAlert.textContent = 'Ops... isto é uma parede!';
      return;
    }

    let newStr0 = '';
    newStr0 = map[playerPosition['linha']].replace('S', ' ');   
    map.splice(playerPosition['linha'], 1, newStr0); 
    playerPosition['linha'] += 1; 

    let newStr1 = map[playerPosition['linha']];
    let strArr = newStr1.split("");
    strArr.splice(playerPosition['coluna'], 1, 'S');
    let newStr2 = strArr.join("");
    map.splice(playerPosition['linha'], 1, newStr2); 

    sketchLab();
  }

}); 

function reload(){
  location.reload();
}






  


